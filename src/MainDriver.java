import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

public class MainDriver {

    //Setting for running as multiple program
    private static int total_thread = 5; //RANGE: 0 < n < 100
    private static int block_num = 1; //RANGE: 0 < n < total_thread
    
    //Setting for connection to DB
    private static String userName = "root";
    private static String password = "";
    private static String database_name = "linkedin_shaohong";
    private static String saved_dir = "C:\\Users\\Shao Hong\\Desktop\\Ke-Wei Project\\output";
    private static int max_html = 1000; 
    
    //JDBC
    private static Connection conn = null;
    private static Statement stmt = null;
    private static ResultSet rs = null;
    private static PreparedStatement preparedStmt = null;
     
    //DO NOT edit anything below
    private static HttpClient httpClient = new HttpClient();
    private static int start_pos, end_pos, block_size;
    private static String url = "jdbc:mysql://localhost/" + database_name;
    private static String folder_name = "save_thread_" + block_num + "_";
    
    //Tracker to ensure new folder will be created without duplicates
    private static int folder_count = 0;
    
    public static void main(String[] args) {
	
	System.out.println("Start of Program.");
	httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
	
	try {
	    check_param();
	    connect_db();
	}catch(Exception ex){
	    ex.printStackTrace();
	    System.out.println("Program exit!");
	    System.exit(0);
	}

	try{
	    while(true){
		getURLForExtract();
        	saveHTMLPage();
        	System.out.println("Program going to sleep");
        	Thread.sleep(300000); //300 Seconds or 5 min
        	System.out.println("Program wakes up");
	    }
	} catch(InterruptedException ex){
	    System.out.println("Thread error");
	}
	
    } //End of main method

    /**
     * @throws Exception 
     * 
     */
    private static void check_param() throws Exception{
	if(total_thread <= 0 || total_thread >= 100){
	    throw new Exception("Please ensure your total_thread val is 0 < n < 100");
	}else if(block_num < 0 || block_num >= 100){
	    throw new Exception("Please ensure your block_num val is 0 <= n < 100");
	}else if(block_num > total_thread){
	    throw new Exception("Please block_num is LESS THAN total_thread");
	}
	block_size = 100 / total_thread;
	end_pos = block_size * block_num;
	start_pos = end_pos - (block_size - 1);
	
    }
    
    /**
     * This is to setup connection to MySQL
     */
    private static void connect_db(){
	try{
	    Class.forName("com.mysql.jdbc.Driver").newInstance();
	    conn = DriverManager.getConnection(url, userName, password);
	    stmt = conn.createStatement();
	    System.out.println("Connected to Database.");
	} catch(Exception ex){
	    System.out.println("Connection to DB error! Please check username/password.");
	}
    } //End of connect_db()

    
    /**
     * This method will get all urls that are not extracted
     */
    private static void getURLForExtract(){
	String query = "SELECT * FROM urls WHERE status = false AND " +
			"rand_num BETWEEN " + start_pos + " AND " + end_pos;
	try {
	    rs = stmt.executeQuery(query);
	} catch (SQLException e) {
	    System.out.println("Query for URLs to extract error! Please check DB.");
	}
    } //End of getURLForExtract()
    
    /**
     * This will update the database to set it as save after it save the
     * html profile.
     * @param url
     */
    private static void alterURLState(String url){
	String query = "UPDATE urls SET status = ? WHERE url = ?";
	try {
	    preparedStmt = conn.prepareStatement(query);
	    preparedStmt.setBoolean(1, true);
	    preparedStmt.setString(2, url);
	    preparedStmt.executeUpdate();
	} catch (SQLException e) {
	    System.out.println("(WARNING) Update URL to extract error!");
	}
    }
    
    /**
     * This will delete malformat url
     * 
     * @param url
     */
    private static void deleteURL(String url){
	String query = "DELETE FROM urls WHERE url = " + url;
	try {
	    stmt.executeQuery(query);
	} catch (SQLException e) {
	    System.out.println("(WARNING) Delete URL to extract error!");
	}
    }

    
    /**
     * This method will save HTML page into folder and it will create folder
     * when it reach the max number of html in each folder.
     */
    private static void saveHTMLPage(){
	String folder_dir = saved_dir + "\\" + folder_name + folder_count;
	String tempFileName = "";
	String[] tempSplit;
	int count = 0;
	try {
	    new File(folder_dir).mkdir();
	    while(rs.next()){
		
		try{
		    String profile_url = rs.getString(1);
		    tempSplit = profile_url.split("/");
		    
		    //This will create filename for the saved html
		    try{
			tempFileName = tempSplit[3] + "_" + tempSplit[4] + "_" + tempSplit[5] + "_" + 
				tempSplit[6] + "_" + tempSplit[7] + "_" + ".html";
		    } catch (ArrayIndexOutOfBoundsException ex){
			deleteURL(rs.getString(1));
		    }
		    
		    String page = getPage(profile_url);
		    writeFile(folder_dir + "\\" + tempFileName, page);
		    alterURLState(profile_url);
		    count++;
		    tempFileName = "";
		} catch (Exception ex) {
		    System.out.println("(WARNING) Error saving and profile skipped.");
		}
		
		//It will create new folder each time it reach the max html in the folder
		if(count > max_html){
		    count = 0;
		    folder_count++;
		    folder_dir = saved_dir + "\\" + folder_name + folder_count;
		    new File(folder_dir).mkdir();
		}
		
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("(WARNING) Error to update status");
	}
    }
    
    /**
     * Write HTML to file
     * 
     * @param save_dir
     * @param data
     * @return
     */
    public static void writeFile(String save_dir, String data) {
	try {
	    BufferedWriter out = new BufferedWriter(new FileWriter(save_dir));
	    out.write(data);
	    out.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Send a POST to get html
     * 
     * @param link
     * @return
     * @throws HttpException
     * @throws IOException
     */
    private static String getPage(String link) throws HttpException,
    IOException {
	GetMethod post = new GetMethod(link);
	httpClient.executeMethod(post);
	String output;
	InputStream stream = post.getResponseBodyAsStream();
	output = StreamToString(stream);
	stream.close();
	post.releaseConnection();
	return output;
    }

    
    private static String StreamToString(InputStream stream) throws IOException {
	BufferedReader reader = new BufferedReader(new InputStreamReader(
		stream, "UTF-8"));
	String line = "";
	String result = "";
	for (line = reader.readLine(); line != null; line = reader.readLine()) {
	    result += line;
	}
	reader.close();
	return result;
    }

    /**
     * DO NOT CHANGE THIS!
     * 
     * @author Shao Hong
     *
     */
    public static class UTF8PostMethod extends PostMethod {
	public UTF8PostMethod(String url) {
	    super(url);
	}

	public String getRequestCharSet() {
	    // return super.getRequestCharSet();
	    return "UTF-8";
	}
    }
    
} //End of class
